@extends('template')   
@section('content') 



<section class="main-section">
                @if(Session::has('alert-success'))
                <div class="alert alert-success">
                    <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
                </div>
            @endif
            <h1>Tambah User</h1>
            <form action="{{ route('user.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}         
               
                <div class="form-group">
                <label for="name">Nama Lengkap:</label>
                @if ($errors->any())
                <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : 'is-valid' }}" id="name" name="name" value="{{ old('name') }}">
                @else
                <input type="text" class="form-control" id="name" name="name">
                @endif
                @if ($errors->has('name'))
                <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                @endif
                </div>   
              
        
                <div class="form-group">
                <label for="email">Email:</label>
                @if ($errors->any())
                <input type="text" class="form-control {{ $errors->has('email') ? 'is-invalid' : 'is-valid' }}" id="email" name="email" value="{{ old('email') }}">
                @else
                <input type="text" class="form-control" id="email" name="email">
                @endif
                @if ($errors->has('email'))
                <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                @endif
                </div>            

                <div class="form-group">
                <label for="password">Password:</label>
                @if ($errors->any())
                <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : 'is-valid' }}" id="password" name="password">
                @else
                <input type="password" class="form-control" id="password" name="password">
                @endif
                @if ($errors->has('password'))
                <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                @endif
                </div>

                <div class="form-group">
                <button type="submit" class="btn btn-md btn-primary">Submit</button>
                <button type="reset" class="btn btn-md btn-danger">Cancel</button>
                </div>
            </form>
        </div>
    </section>
    @endsection
    
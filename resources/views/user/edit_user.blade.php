@extends('template')
@section('content')

<section class="main-section">
        
            <h1>Edit User</h1>
            <hr>
            @foreach($data as $datas)
            <form action="{{ route('user.update', $datas->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}                    
                          {{ method_field('PUT') }}    
             @if (isset($data))
            <input type="hidden" id="id" name="id" value="{{ $datas->id }}">                          
            @endif
          
            <div class="form-group">
            <label for="name">Nama Lengkap:</label>
            @if ($errors->any())
            <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : 'is-valid' }}" id="name" name="name" value="{{ old('name') }}">
            @else
            <input type="text" class="form-control" id="name" name="name" value="{{ $datas->name }}">
            @endif
            @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
            @endif
            </div>    

            <div class="form-group">
            <label for="email">Email:</label>
            @if ($errors->any())
            <input type="text" class="form-control {{ $errors->has('email') ? 'is-invalid' : 'is-valid' }}" id="email" name="email" value="{{ old('email') }}">
            @else
            <input type="text" class="form-control" id="email" name="email" value="{{ $datas->email }}">
            @endif
            @if ($errors->has('email'))
            <div class="invalid-feedback">{{ $errors->first('email') }}</div>
            @endif
            </div>    
            
            <div class="form-group">
            <label for="password">Password:</label>
            @if ($errors->any())
            <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : 'is-valid' }}" id="password" name="password">
            @else
            <input type="password" class="form-control" id="password" name="password">
            @endif
            @if ($errors->has('password'))
            <div class="invalid-feedback">{{ $errors->first('password') }}</div>
            @endif
            </div>
            
            <div class="form-group">
            <button type="submit" class="btn btn-md btn-primary">Submit</button>
            <button type="reset" class="btn btn-md btn-danger">Cancel</button>
            </div>
            </form>
            @endforeach
</section>
@endsection
@extends('template')   
@section('content') 

<section class="main-section">
    <div class="content col-md-12 col-sm-12 col-12">
            <h1>User</h1>            
            @if(Session::has('alert-success'))
            <div class="alert alert-success">
                <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
            </div>
        @endif
        <table class="table table-hover">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Lengkap</th>
                    <th>Email</th>                    
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @php $no = 1; @endphp
                @foreach($data as $datas)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $datas->name }}</td>       
                        <td>{{ $datas->email }}</td>                                  
                        <td>
                            <form action="{{ route('user.destroy', $datas->id) }}" method="post">
                                {{ csrf_field() }}                                                                    
                                {{ method_field('DELETE') }}
                                <a href="{{ route('user.edit',$datas->id) }}" class=" btn btn-sm btn-primary">Edit</a>
                                <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
         
            <a href="/user/create" class="btn btn-sm btn-success">Tambah User</a>            
    </div>
</section>
@endsection


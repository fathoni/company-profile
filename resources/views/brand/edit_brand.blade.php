@extends('template')
@section('content')
            <h1>Edit Brand</h1>
      
            @foreach($data as $datas)
            <form action="{{ route('brand.update', $datas->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}                    
                    {{ method_field('PUT') }}            

                <div class="form-group">
                <label for="brand_name">Nama Brand:</label>
                @if ($errors->any())
                <input type="text" class="form-control {{ $errors->has('brand_name') ? 'is-invalid' : 'is-valid' }}" id="brand_name" name="brand_name" value="{{ old('brand_name') }}">
                @else
                <input type="text" class="form-control" id="brand_name" name="brand_name" value="{{ $datas->brand_name }}">
                @endif
                @if ($errors->has('brand_name'))
                <div class="invalid-feedback">{{ $errors->first('brand_name') }}</div>
                @endif
                </div>    

                <div class="form-group">
                <label for="brand_foto">Brand Logo:</label>
                @if ($errors->any())
                <input type="file" class=" form-control {{ $errors->has('brand_foto') ? 'is-invalid' : 'is-valid' }}" id="brand_foto" name="brand_foto" >
                @else
                <input type="file" class="form-control" id="brand_foto" name="brand_foto">  
                @endif      
                @if ($errors->has('brand_foto'))
                <div class="invalid-feedback">{{ $errors->first('brand_foto') }}</div>
                @endif
                </div>
                

                <div class="form-group">
                     <button type="submit" class="btn btn-md btn-primary">Submit</button>
                     <button type="reset" class="btn btn-md btn-danger">Cancel</button>
                </div>
            </form>
            @endforeach
@endsection
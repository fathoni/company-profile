@extends('template')   
@section('content') 


<h1>Tambah Category</h1>
<form action="{{ route('category.store') }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}      
    
    <div class="form-group">
    <label for="category_name">Category:</label>
    @if ($errors->any())
    <input type="text" class="form-control {{ $errors->has('category_name') ? 'is-invalid' : 'is-valid' }}" id="category_name" name="category_name" value="{{ old('category_name') }}">
    @else
    <input type="text" class="form-control" id="category_name" name="category_name" >
    @endif
    @if ($errors->has('category_name'))
    <div class="invalid-feedback">{{ $errors->first('category_name') }}</div>
    @endif
    </div> 

    <div class="form-group">
        <button type="submit" class="btn btn-md btn-primary">Submit</button>
        <button type="reset" class="btn btn-md btn-danger">Cancel</button>
    </div>
    </form>

@endsection
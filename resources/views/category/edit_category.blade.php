@extends('template')
@section('content')
            <h1>Edit Category</h1>
      
            @foreach($data as $datas)
            <form action="{{ route('category.update', $datas->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}                    
                    {{ method_field('PUT') }}            

                <div class="form-group">
                <label for="category_name">Cateegory:</label>
                @if ($errors->any())
                <input type="text" class="form-control {{ $errors->has('category_name') ? 'is-invalid' : 'is-valid' }}" id="category_name" name="category_name" value="{{ old('category_name') }}">
                @else
                <input type="text" class="form-control" id="category_name" name="category_name" value="{{ $datas->category_name }}">
                @endif
                @if ($errors->has('category_name'))
                <div class="invalid-feedback">{{ $errors->first('category_name') }}</div>
                @endif
                </div>   
                

                <div class="form-group">
                     <button type="submit" class="btn btn-md btn-primary">Submit</button>
                     <button type="reset" class="btn btn-md btn-danger">Cancel</button>
                </div>
            </form>
            @endforeach
@endsection
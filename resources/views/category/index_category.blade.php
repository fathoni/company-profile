@extends('template')   
@section('content') 


<section class="main-section">
        <div class="content">
                @if(Session::has('alert-success'))
                <div class="alert alert-success">
                    <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
                </div>
            @endif
            <h1>Category</h1>        
            <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Category </th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $no = 1; @endphp
                    @foreach($data as $datas)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $datas->category_name }}</td>
                            <td>
                                <form action="{{ route('category.destroy', $datas->id) }}" method="post">
                                    {{ csrf_field() }}                                                                    
                                    {{ method_field('DELETE') }}
                                    <a href="{{ route('category.edit',$datas->id) }}" class=" btn btn-sm btn-primary">Edit</a>
                                    <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
    
                <a href="/category/create" class="btn btn-sm btn-success">Add category</a>
                
        </div>
    </section>
@endsection
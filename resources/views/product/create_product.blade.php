@extends('template')   
@section('content') 


<h1>Add Product</h1>
<form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}      
   

    <div class="form-group">
    <label for="product_name">Nama Product:</label>
    @if ($errors->any())
    <input type="text" class="form-control {{ $errors->has('product_name') ? 'is-invalid' : 'is-valid' }}" id="product_name" name="product_name" value="{{ old('product_name') }}">
    @else
    <input type="text" class="form-control" id="product_name" name="product_name">
    @endif
    @if ($errors->has('product_name'))
    <div class="invalid-feedback">{{ $errors->first('product_name') }}</div>
    @endif
    </div>   

    <div class="form-group">
    <label for="product_spec">Specifikation:</label>
    @if ($errors->any())
    <input type="text" class="form-control {{ $errors->has('product_spec') ? 'is-invalid' : 'is-valid' }}" id="product_spec" name="product_spec" value="{{ old('product_spec') }}">
    @else
    <input type="text" class="form-control" id="product_spec" name="product_spec">
    @endif
    @if ($errors->has('product_spec'))
    <div class="invalid-feedback">{{ $errors->first('product_spec') }}</div>
    @endif
    </div>   

    <div class="form-group">
    <label for="id_brand">Brand:</label>
    @if ($errors->any())            
    <select name="id_brand" class="custom-select {{ $errors->has('id_brand') ? 'is-invalid' : 'id_brand' }}" id="id_brand" >
    @else
    <select name="id_brand" class="custom-select"> 
    @endif               
    <option selected disabled hidden>Pilih</option>
    @foreach ($brand as $brans) 
    <option value="{{ $brans->id }}">{{ $brans->brand_name  }}</option>
    @endforeach
    </select>
    @if ($errors->has('id_brand'))            
    <div class="invalid-feedback">{{ $errors->first('id_brand') }}</div>
    @endif
    </div> 

    <div class="form-group">
    <label for="transmision">Transmision:</label>
    @if ($errors->any())
    <input type="text" class="form-control {{ $errors->has('transmision') ? 'is-invalid' : 'is-valid' }}" id="transmision" name="transmision" value="{{ old('transmision') }}">
    @else
    <input type="text" class="form-control" id="transmision" name="transmision">
    @endif
    @if ($errors->has('transmision'))
    <div class="invalid-feedback">{{ $errors->first('transmision') }}</div>
     @endif
    </div>   

    <div class="form-grop">
    <label for="category_name">Category:</label>
    @foreach($category as $categorys)
    <div class="checkbox">
    
    <input type="checkbox" name="category_name[]" value="{{$categorys->id}}"> {{$categorys->category_name}}<br>
          
    </div>
    @endforeach
    </div>

    
        <div class="form-group fieldGroup">
                <div class="input-group">
                    <input type="text" name="color[]" class="form-control" placeholder="Warna"/>
                    <input type="file" name="icon_color[]" class="form-control" placeholder="Icon color"/>
                    <input type="file" name="foto_mobil[]" class="form-control" placeholder="Foto Mobil"/>
                    <input type="text" name="harga[]" class="form-control" placeholder="Harga"/>                    
                    <div class="input-group-addon"> 
                        <a href="javascript:void(0)" class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> +</a>
                    </div>
                </div>
            </div>
            


    <div class="form-group">
        <button type="submit" class="btn btn-md btn-primary submit">Submit</button>
        <button type="reset" class="btn btn-md btn-danger">Cancel</button>
    </div>
</form>
<div class="form-group fieldGroupCopy" style="display: none;">
        <div class="input-group">
            <input type="text" name="color[]" class="form-control" placeholder="Warna"/>
            <input type="text" name="icon_color[]" class="form-control" placeholder="Icon color"/>
            <input type="text" name="foto_mobil[]" class="form-control" placeholder="Foto Mobil"/>
            <input type="text" name="harga[]" class="form-control" placeholder="Harga"/>            
            <div class="input-group-addon"> 
                <a href="javascript:void(0)" class="btn btn-danger remove"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> -</a>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){
    //group add limit
    var maxGroup = 10;
    
    //add more fields group
    $(".addMore").click(function(){
        if($('body').find('.fieldGroup').length < maxGroup){
            var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
            $('body').find('.fieldGroup:last').after(fieldHTML);
        }else{
            alert('Maximum '+maxGroup+' groups are allowed.');
        }
    });
    
    //remove fields group
    $("body").on("click",".remove",function(){ 
        $(this).parents(".fieldGroup").remove();
    });
});
        </script>
@endsection




@extends('template')
@section('content')
            <h1>Edit Product</h1>
      
            @foreach($data as $datas)
            <form action="{{ route('brand.update', $datas->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}                    
                    {{ method_field('PUT') }}            

                    <div class="form-group">
                    <label for="product_name">Nama Product:</label>
                    @if ($errors->any())
                    <input type="text" class="form-control {{ $errors->has('product_name') ? 'is-invalid' : 'is-valid' }}" id="product_name" name="product_name" value="{{ old('product_name') }}">
                    @else
                    <input type="text" class="form-control" id="product_name" name="product_name" value="{{ $datas->product_name }}">
                    @endif
                    @if ($errors->has('product_name'))
                    <div class="invalid-feedback">{{ $errors->first('product_name') }}</div>
                    @endif
                    </div>    

                    <div class="form-group">
                    <label for="product_spec">Specification :</label>
                    @if ($errors->any())
                    <input type="text" class="form-control {{ $errors->has('product_spec') ? 'is-invalid' : 'is-valid' }}" id="product_spec" name="product_spec" value="{{ old('product_spec') }}">
                    @else
                    <input type="text" class="form-control" id="product_spec" name="product_spec" value="{{ $datas->product_spec }}">
                    @endif
                    @if ($errors->has('product_spec'))
                    <div class="invalid-feedback">{{ $errors->first('product_spec') }}</div>
                    @endif
                    </div>    
        
                    <div class="form-group">
                    <label for="id_brand">Brand:</label>
                    @if ($errors->any())            
                    <select name="id_brand" class="custom-select {{ $errors->has('id_brand') ? 'is-invalid' : 'id_brand' }}" id="id_brand" >
                    @else
                    <select name="id_brand" class="custom-select"> 
                    @endif               
                    <option selected disabled hidden>Pilih</option>
                    @foreach ($brand as $brans) 
                    <option value="{{ $brans->id }}">{{ $brans->brand_name  }}</option>
                    @endforeach
                    </select>
                    @if ($errors->has('id_brand'))            
                    <div class="invalid-feedback">{{ $errors->first('id_brand') }}</div>
                    @endif
                    </div> 

                <div class="form-group">
                     <button type="submit" class="btn btn-md btn-primary">Submit</button>
                     <button type="reset" class="btn btn-md btn-danger">Cancel</button>
                </div>
            </form>
            @endforeach
@endsection
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetailProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('detail_product', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('product_id');
        $table->string('color');
        $table->string('icon_color');
        $table->string('foto_mobil');
        $table->string('harga');
        $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

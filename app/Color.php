<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Color extends Model
{
    protected $table = 'color';
    protected $fillable = ['color'];

    public function product(){
        return $this->belongsToMany('App\Product');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';        
    protected $fillable = ['product_name', 'product_spec', 'id_brand','transmision'];
   
    public function brand(){
        return $this->belongsTo('App\Brand', 'id_brand');
    }
    public function category(){
        return $this->belongsToMany('App\Category');
    }
    public function detail_product(){
        return $this->belongsToMany('App\DetailProduct');
    }
}

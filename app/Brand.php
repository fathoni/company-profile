<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brand';    
    protected $fillable = ['brand_name','brand_foto'];
    
    public function product(){
        return $this->hasMany('App\Product', 'id_brand');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailProduct extends Model
{
    protected $table = 'detail_product';
    protected $fillable = ['color', 'icon_color', 'foto_mobil', 'harga'];

    public function product(){
        return $this->belongsToMany('App\Product');
    }
}

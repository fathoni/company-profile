<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;



class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        {
            $data = User::paginate(10);
            return view('user.user', compact('data'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.add_user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $data = $request->all();        
         $validasi = Validator::make($data, [
            'email'    => 'required|email|unique:users',
            'name'     => 'required|max:255',
            'password' => 'required|min:6',
          
        ]);
        if ($validasi->fails()) {
            return redirect('user/create')
                    ->withInput()
                    ->withErrors($validasi);
        }

        $data['password'] = bcrypt($data['password']);      
        $user=User::create($data);
        return redirect()->route('user.index')->with('alert-success','Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::where('id',$id)->get();
        return view('user.edit_user', compact('data','departement'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $data = $request->all();


        $validasi = Validator::make($data, [
            'email'      => 'required|email|unique:users,email,'. $data['id'], 
            'name'     => 'required|max:255',
            'password' => 'required|min:6',
         
        ]);
        
        if ($validasi->fails()) {
            return redirect("user/$id/edit")
                    ->withInput()
                    ->withErrors($validasi);
        }

        if ($request->has('password')) {
            // Hash password.
            $data['password'] = bcrypt($data['password']);
        } else {
            // Hapus password (password tidak diupdate).
            $data = array_except($data, ['password']);
        }

        $user->update($data);
        return redirect()->route('user.index')->with('alert-success','Berhasil Update Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::where('id',$id)->first();
        $data->delete();
        return redirect()->route('user.index')->with('alert-success','Data berhasi dihapus!');
    }
}

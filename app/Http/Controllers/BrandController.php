<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use Validator;
use Storage;



class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Brand::all();
        return view('brand.index_brand', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('brand.create_brand');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
        $data = $request->all();
        $validasi = Validator::make($data, [
            'brand_name'      => 'required|unique:brand',
            'brand_foto'      => 'required|image|mimes:jpeg,jpg,bmp,png'
            
        ]);
        if ($validasi->fails()) {
            return redirect('brand/create')
                    ->withInput()
                    ->withErrors($validasi);
        }
        // if ($request->hasFile('brand_foto')){
            // $this->hapusFoto($img);
            $data['brand_foto']= $this->uploadFoto($request);
            
        // }
        Brand::create($data);
        return redirect()->route('brand.index')->with('alert-success','Berhasil Menambahkan Data!');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Brand::where('id',$id)->get();        
        return view('brand.edit_brand', compact('data'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = Brand::findOrFail($id);
        $img = Brand::where('id',$id)->first();                
        $data = $request->all();

        $validasi = Validator::make($data, [
            'brand_name'      => 'required|unique:brand,brand_name,'. $brand->id,
            'brand_foto'      => 'sometimes|image|mimes:jpeg,jpg,bmp,png'
        ]); 
        if ($validasi->fails()) {
            return redirect('brand/create')
                    ->withInput()
                    ->withErrors($validasi);
        }
        if ($request->hasFile('brand_foto')){
            $this->hapusFoto($img);
            $data['brand_foto']= $this->uploadFoto($request);
            
        }
        
        $brand->update($data);
        return redirect()->route('brand.index')->with('alert-success','Berhasil Update Data!');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Brand::where('id',$id)->first();
        $this->hapusFoto($data);        
        $data->delete();
        return redirect()->route('brand.index')->with('alert-success','Data berhasi dihapus!');   
    }

    private function uploadFoto( $request)
	{
		$brand_foto = $request->file('brand_foto');
        $filename = $brand_foto->getClientOriginalName();
        
		if ($request->file('brand_foto')->isValid()) {
            $foto_name   = date('YmdHis'). ".$filename";            
            $request->file('brand_foto')->move("gambar/", $foto_name);
            
			return $foto_name;
			}
			return false;
    }
    
    private function hapusFoto($img)
    {
          $exist = Storage::disk('brand_foto')->exists($img->brand_foto);
  
          if (isset($img->brand_foto) && $exist) {
              $delete = Storage::disk('brand_foto')->delete($img->brand_foto);
              if ($delete) {
                  return true;
              }
              return false;
          }
      }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Validator;


class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::all();
        return view('category.index_category', compact('data'));
    }

    public function create()
    {
        return view('category.create_category');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validasi = Validator::make($data, [
            'category_name'      => 'required|unique:category',
            
        ]);
        if ($validasi->fails()) {
            return redirect('category/create')
                    ->withInput()
                    ->withErrors($validasi);
        }
        Category::create($data);
        return redirect()->route('category.create')->with('alert-success','Berhasil Menambahkan Data!');        
    
    }

    public function edit()
    {
        $data = Category::all();
        return view('category.edit_category', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $data = $request->all();

        $validasi = Validator::make($data, [
            'category_name'      => 'required|unique:category,category_name,'. $category->id,
        ]); 
        if ($validasi->fails()) {
            return redirect('category/create')
                    ->withInput()
                    ->withErrors($validasi);
        }
        $category->update($data);
        return redirect()->route('category.index')->with('alert-success','Berhasil Update Data!');        
    }

    public function destroy($id)
    {
        $data = Category::where('id',$id)->first();      
        $data->delete();
        return redirect()->route('category.index')->with('alert-success','Data berhasi dihapus!');   
    }
}

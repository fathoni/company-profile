<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\Product;
use App\Category;
use App\DetailProduct;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Product::all();
        return view('product.index_product', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand = Brand::all();
        $category = Category::all();
        return view('product.create_product', compact('brand','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $product=Product::create($data);
        $category = Category::find($data['category_name']);
        $product->category()->attach($category);
        $color = $data['color'];
       foreach ($color as $key => $color){ 
        $detail_product = new DetailProduct;
        $detail_product->product_id = $product['id'];
        $detail_product->color = $data['color'][$key];        
        $detail_product->icon_color = $data['icon_color'][$key]; 
        $detail_product->foto_mobil = $data['foto_mobil'][$key];        
        $detail_product->harga = $data['harga'][$key];        
        $product->detail_product()->save($detail_product);
       }
      
        // return redirect()->route('product.index_product')->with('alert-success','Berhasil Menambahkan Data!');       
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Product::where('id',$id)->get();   
        $brand = Brand::all();        
        return view('product.edit_product', compact('data','brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
